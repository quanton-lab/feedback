<?php

namespace QuantonLab\Feedback\Exceptions;

use Exception;

class TooHighNoteException extends Exception
{
}
