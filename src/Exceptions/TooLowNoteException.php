<?php

namespace QuantonLab\Feedback\Exceptions;

use Exception;

class TooLowNoteException extends Exception
{
}
