<?php

namespace QuantonLab\Feedback;

use Illuminate\Support\ServiceProvider;
use QuantonLab\Feedback\Model\EloquentFeedback;
use QuantonLab\Feedback\Model\Feedback;
use QuantonLab\Feedback\Services\DefaultFeedbackService;
use QuantonLab\Feedback\Services\FeedbackService;

class FeedbackServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        FeedbackService::class => DefaultFeedbackService::class,
        Feedback::class => EloquentFeedback::class,
    ];

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    public function register()
    {
        //
    }
}
