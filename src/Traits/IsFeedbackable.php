<?php

namespace QuantonLab\Feedback\Traits;

use Illuminate\Support\Collection;
use QuantonLab\Feedback\Model\EloquentFeedback;

/**
 * Use this trait on an Eloquent model to implement the Feedbackable interface. 
 */
trait IsFeedbackable
{
    public function feedbacks()
    {
        return $this->morphMany(EloquentFeedback::class, 'feedbackable');
    }

    public function getFeedbacks(): Collection
    {
        return $this->feedbacks()->get();
    }

    public function getAverageNote()
    {
        return $this->getFeedbacks()->average('note');
    }
}
