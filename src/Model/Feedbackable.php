<?php

namespace QuantonLab\Feedback\Model;

use Illuminate\Support\Collection;

interface Feedbackable
{
    public function getFeedbacks(): Collection;

    public function getAverageNote();
}
