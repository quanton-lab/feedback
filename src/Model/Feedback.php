<?php

namespace QuantonLab\Feedback\Model;

/**
 * Feedback interface
 */
interface Feedback
{
    public function getNote(): int;

    public function setNote(int $note): self;

    public function getComment(): ?string;

    public function setComment(string $comment): self;

    public function save();

    public function getFeedbackable(): Feedbackable;

    public function setFeedbackable(Feedbackable $feedbackable): self;

    public function getFeedbacker(): ?Feedbacker;

    public function setFeedbacker(Feedbacker $feedbacker): self;
}
