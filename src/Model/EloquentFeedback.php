<?php

namespace QuantonLab\Feedback\Model;

use Illuminate\Database\Eloquent\Model;
use QuantonLab\Feedback\Exceptions\TooHighNoteException;
use QuantonLab\Feedback\Exceptions\TooLowNoteException;

class EloquentFeedback extends Model implements Feedback
{
    const TABLE_NAME = 'feedback_feedbacks';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    public function setNote(int $note): Feedback
    {
        if ($note > 5) {
            throw new TooHighNoteException();
        }
        if ($note < 0) {
            throw new TooLowNoteException();
        }

        $this->note = $note;

        return $this;
    }

    public function getNote(): int
    {
        return $this->note;
    }

    public function setComment(string $comment): Feedback
    {
        $this->comment = $comment;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function feedbackable()
    {
        return $this->morphTo();
    }

    public function getFeedbackable(): Feedbackable
    {
        return $this->feedbackable;
    }

    public function setFeedbackable(Feedbackable $feedbackable): Feedback
    {
        $this->feedbackable()->associate($feedbackable);

        return $this;
    }

    public function feedbacker()
    {
        return $this->morphTo();
    }

    public function getFeedbacker(): ?Feedbacker
    {
        return $this->feedbacker;
    }

    public function setFeedbacker(Feedbacker $feedbacker): Feedback
    {
        $this->feedbacker()->associate($feedbacker);

        return $this;
    }
}
