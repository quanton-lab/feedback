<?php

namespace QuantonLab\Feedback\Services;

use QuantonLab\Feedback\Model\Feedback;
use QuantonLab\Feedback\Model\Feedbackable;
use QuantonLab\Feedback\Model\Feedbacker;

class DefaultFeedbackService implements FeedbackService
{
    public function make(Feedbackable $feedbackable, int $note, string $comment = null, Feedbacker $feedbacker = null): Feedback
    {
        /**
         * @var Feedback $feedback
         */
        $feedback = resolve(Feedback::class);

        $feedback->setFeedbackable($feedbackable);
        $feedback->setNote($note);
        if (isset($comment) && $comment != '') {
            $feedback->setComment($comment);
        }
        if (isset($feedbacker)) {
            $feedback->setFeedbacker($feedbacker);
        }

        return $feedback;
    }

    public function create(Feedbackable $feedbackable, int $note, string $comment = null, Feedbacker $feedbacker = null): Feedback
    {
        $feedback = $this->make($feedbackable, $note, $comment, $feedbacker);
        $feedback->save();

        return $feedback;
    }
}
