<?php

namespace QuantonLab\Feedback\Services;

use QuantonLab\Feedback\Model\Feedback;
use QuantonLab\Feedback\Model\Feedbackable;
use QuantonLab\Feedback\Model\Feedbacker;

interface FeedbackService
{
    public function make(Feedbackable $feedbackable, int $note, string $comment = null, Feedbacker $feedbacker = null): Feedback;

    public function create(Feedbackable $feedbackable, int $note, string $comment = null, Feedbacker $feedbacker = null): Feedback;
}
