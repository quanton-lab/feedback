<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use QuantonLab\Feedback\Model\EloquentFeedback;

class createFeedbackTable extends Migration
{
    public function up()
    {
        Schema::create(EloquentFeedback::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->integer('feedbackable_id')->unsigned()->nullable();
            $table->string('feedbackable_type')->nullable();

            $table->integer('feedbacker_id')->unsigned()->nullable();
            $table->string('feedbacker_type')->nullable();

            // $table->bigInteger('business_id')->unsigned();
            $table->integer('note')->unsigned()->nullable();
            $table->text('comment')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists(EloquentFeedback::TABLE_NAME);
    }
}
