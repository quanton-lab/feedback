# Feedback

This is a really simple package that ables you to handle Feedback. 

It comes out of the box with an Eloquent implementation but feel free to create your own custom implementation of the interfaces. (See below)

## Get Started

### Install with composer
Run `composer require quanton-lab/feedback`.  

### Implement `Feedbackable`
An object from your model should implement the Feedbackable interface in order to be associated with notes and comments. 
Note that you can use the trait to actually implement the methods required by the interface. 

### Implement `Feedbacker`
An object from you model should implement the Feedbacker interface if you want to keep track of who gave which notes.  Generally speaking it will be some kind of user.  
This interface is not mandatory.  

### Use the `FeedbackService`

#### Create feedback
Make a feedback on a feedbackable item with the service, returns the instance of the feedback.  Then you can save it.  
```php
$feedback = $feedbackService->make(Feedbackable $feedbackable, $note, $comment, Feedbacker $feedbacker);

$feedback->save();
```
Or you can simply create it so you don't have to save it yourself. 
```php
$feedback = $feedbackService->create(Feedbackable $feedbackable, $note, $comment, Feedbacker $feedbacker);
```

Once you have the `$feedback` instance, you can edit it with the setters methods and then save your changes.  

### Retrieve feedbacks
On a `Feedbackable` instance, you can retrieve all associated feedbacks as well as the average note

```php
$avgNote = $feedbackable->getAverageNote();

$feedbacks = $feedbackable->getFeedbacks();
```

## If you're not working with Eloquent
This package provides you interfaces that you can implement any other way you want.  
1. Write your own implementation of `FeedbackService`and `Feedback`.  
2. Note that a `Feedbackable`item cannot be implementent with the use of the Trait, you'll have to write your own. 
3. Publish FeedbackService provider and bind your own implementation of `FeedbackService` and `Feedback` to the container. 
