<?php

namespace QuantonLab\Feedback\Tests;

use QuantonLab\Feedback\Model\EloquentFeedback;
use QuantonLab\Feedback\Model\Feedbackable;
use QuantonLab\Feedback\Traits\IsFeedbackable;

class FeedbackableTestImplementation implements Feedbackable
{
    use IsFeedbackable;
}


class IsFeedbackableTest extends TestCase
{
    /**
     * @var Mockery/MockInterface
     */
    protected $feedbackable;

    public function setUp(): void
    {
        parent::setUp();

        $this->feedbackable = $this->partialMock(FeedbackableTestImplementation::class);
    }

    public function testGetAverageNoteMethod()
    {
        $collection = collect([
            (new EloquentFeedback())->setNote(1),
            (new EloquentFeedback())->setNote(3),
            (new EloquentFeedback())->setNote(5),
            (new EloquentFeedback())->setNote(4),
            (new EloquentFeedback())->setNote(3),
        ]);

        $this->feedbackable->shouldReceive('getFeedbacks')->andReturn($collection);

        $this->assertEquals(3.2, $this->feedbackable->getAverageNote());
    }
}
