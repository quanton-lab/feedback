<?php

namespace QuantonLab\Feedback\Tests;

use QuantonLab\Feedback\Exceptions\TooHighNoteException;
use QuantonLab\Feedback\Exceptions\TooLowNoteException;
use QuantonLab\Feedback\Model\EloquentFeedback;
use QuantonLab\Feedback\Model\Feedback;
use QuantonLab\Feedback\Model\Feedbackable;
use QuantonLab\Feedback\Model\Feedbacker;

class EloquentFeedbackTest extends TestCase
{
    /**
     * @var Feedback
     */
    protected $feedback;

    public function setUp(): void
    {
        parent::setUp();

        $this->feedback = new EloquentFeedback();
    }

    public function testNoteCannotBeHigherThanFive()
    {
        $note = 6;
        $this->expectException(TooHighNoteException::class);

        $this->feedback->setNote($note);
    }

    public function testNoteCannotBeLowerThanZero()
    {
        $note = -1;
        $this->expectException(TooLowNoteException::class);

        $this->feedback->setNote($note);
    }

    public function testSetNoteAndGetNoteAreCoherent()
    {
        $note = 4;

        $this->feedback->setNote($note);

        $this->assertEquals($note, $this->feedback->getNote());
    }

    public function testSetCommentAndGetCommentAreCoherent()
    {
        $comment = 4;

        $this->feedback->setComment($comment);

        $this->assertEquals($comment, $this->feedback->getComment());
    }

    public function testSetFeedbackableAndGetFeedbackableAreCoherent()
    {
        $feedbackable = $this->mock(Feedbackable::class);

        $this->feedback->setFeedbackable($feedbackable);

        $this->assertEquals($feedbackable, $this->feedback->getFeedbackable());
    }

    public function testSetFeedbackerAndGetFeedbackerAreCoherent()
    {
        $feedbacker = $this->mock(Feedbacker::class);

        $this->feedback->setFeedbacker($feedbacker);

        $this->assertEquals($feedbacker, $this->feedback->getFeedbacker());
    }

    public function testGetFeedbackerCanReturnNull()
    {
        $shouldBeNull = $this->feedback->getFeedbacker();

        $this->assertNull($shouldBeNull);
    }

    public function testGetCommentCanReturnNull()
    {
        $shouldBeNull = $this->feedback->getComment();

        $this->assertNull($shouldBeNull);
    }
}
