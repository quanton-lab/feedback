<?php

namespace QuantonLab\Feedback\Tests;

use QuantonLab\Feedback\Model\Feedback;
use QuantonLab\Feedback\Model\Feedbackable;
use QuantonLab\Feedback\Model\Feedbacker;
use QuantonLab\Feedback\Services\DefaultFeedbackService;
use QuantonLab\Feedback\Services\FeedbackService;

class DefaultFeedbackServiceTest extends TestCase
{
    /**
     * @var FeedbackService
     */
    protected $feedbackService;

    /**
     * @var Mockery/MockInterface
     */
    protected $feedbackMock;

    /**
     * @var Mockery/MockInterface
     */
    protected $feedbackableMock;

    /**
     * @var Mockery/MockInterface
     */
    protected $feedbackerMock;

    public function setUp(): void
    {
        parent::setUp();

        $this->feedbackService = new DefaultFeedbackService();
        $this->feedbackMock = $this->partialMock(Feedback::class);
        $this->feedbackableMock = $this->mock(Feedbackable::class);
        $this->feedbackerMock = $this->mock(Feedbacker::class);
    }

    public function testMakeMethod()
    {
        $note = 4;
        $comment = 'String';

        $this->feedbackMock->shouldReceive('setNote')->with($note)->once();
        $this->feedbackMock->shouldReceive('setComment')->with($comment)->once();
        $this->feedbackMock->shouldReceive('setFeedbackable')->with($this->feedbackableMock)->once();
        $this->feedbackMock->shouldReceive('setFeedbacker')->with($this->feedbackerMock)->once();

        $this->feedbackService->make($this->feedbackableMock, $note, $comment, $this->feedbackerMock);
    }

    public function testCreateMethod()
    {
        $note = 3;
        $comment = 'This is a comment';

        $this->feedbackMock->shouldReceive('setNote')->with($note)->once();
        $this->feedbackMock->shouldReceive('setComment')->with($comment)->once();
        $this->feedbackMock->shouldReceive('setFeedbackable')->with($this->feedbackableMock)->once();
        $this->feedbackMock->shouldReceive('setFeedbacker')->with($this->feedbackerMock)->once();
        $this->feedbackMock->shouldReceive('save')->once()->andReturn($this->feedbackMock);

        $this->feedbackService->create($this->feedbackableMock, $note, $comment, $this->feedbackerMock);
    }
}
