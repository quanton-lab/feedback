<?php

namespace QuantonLab\Feedback\Tests\Model;

use Illuminate\Database\Eloquent\Model;
use QuantonLab\Feedback\Model\Feedbacker;

class TestFeedbacker extends Model implements Feedbacker
{
    const TABLE_NAME = 'feedbackers';

    /**
     * Table name
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;
}
