<?php

namespace QuantonLab\Feedback\Tests\Model;

use Illuminate\Database\Eloquent\Model;
use QuantonLab\Feedback\Model\Feedbackable;
use QuantonLab\Feedback\Traits\IsFeedbackable;

class TestFeedbackable extends Model implements Feedbackable
{
    use IsFeedbackable;

    const TABLE_NAME = 'feedbackables';

    /**
     * Table name
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;
}
