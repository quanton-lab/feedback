<?php

namespace QuantonLab\Feedback\Tests;

use QuantonLab\Feedback\Services\FeedbackService;
use QuantonLab\Feedback\Tests\Model\TestFeedbackable;
use QuantonLab\Feedback\Tests\Model\TestFeedbacker;

class IntegrationTest extends TestCase
{
    /**
     * @var FeedbackService
     */
    protected $feedbackService;

    public function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/database');
        $this->artisan('migrate', ['--database' => 'testbench'])->run();

        $this->feedbackService = resolve(FeedbackService::class);
    }

    public function testFeedbackServiceCreateMethod()
    {
        $feedbackable = TestFeedbackable::create();
        $note = 3;
        $comment = 'Hello';
        $feedbacker = TestFeedbacker::create();

        $this->feedbackService->create($feedbackable, $note, $comment, $feedbacker);

        $this->assertDatabaseHas('feedback_feedbacks', [
            'feedbackable_id' => $feedbackable->id,
            'note' => $note,
            'comment' => $comment,
            'feedbacker_id' => $feedbacker->id,
        ]);
    }

    public function testFeedbacksCanBeRetrievedFromFeedbackable()
    {
        $feedbackable = TestFeedbackable::create();
        $note = 3;
        $comment = 'Hello';
        $feedbacker = TestFeedbacker::create();
        $this->feedbackService->create($feedbackable, $note, $comment, $feedbacker);

        $feedbacks = $feedbackable->getFeedbacks();

        $this->assertCount(1, $feedbacks);
    }

    public function testFeedbackableAverageNote()
    {
        $feedbackable = TestFeedbackable::create();
        $comment = 'Hello';
        $feedbacker = TestFeedbacker::create();
        $this->feedbackService->create($feedbackable, 4, $comment, $feedbacker);
        $this->feedbackService->create($feedbackable, 2, $comment, $feedbacker);
        $this->feedbackService->create($feedbackable, 1, $comment, $feedbacker);

        $avgNote = $feedbackable->getAverageNote();

        $this->assertEquals(7 / 3, $avgNote);
    }
}
