<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use QuantonLab\Feedback\Tests\Model\TestFeedbackable;
use QuantonLab\Feedback\Tests\Model\TestFeedbacker;

class TestingMigrations extends Migration
{
    public function up()
    {
        Schema::create(TestFeedbacker::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create(TestFeedbackable::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists(TestFeedbacker::TABLE_NAME);
        Schema::dropIfExists(TestFeedbackable::TABLE_NAME);
    }
}
